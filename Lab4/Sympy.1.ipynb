{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sympy - A first glance\n",
    "\n",
    "Sympy is a Python library that primarily deals with **symbolic mathematics**.  By this we mean formal algebraic expressions, such as:\n",
    "\n",
    " * Polynomials $x^3+3x-2$\n",
    " * Abstract functional expressions $\\sin(x), e^x, \\frac{\\sin x}{\\tan^2 x + 3x^2 + 1}$, etc.\n",
    " * Operations on these expressions, such as: \n",
    "   - Root finding, in **closed form** when it is possible.\n",
    "   - Differentiation, integration.\n",
    "   - Solving systems of non-linear equations in \"closed form\" i.e. going from expressions like $x^2=2$ to $x = \\pm \\sqrt{2}$. \n",
    "   - Finding **closed form** solutions to differential equations, when they exist, such as going from $y'=y$ to $y(t) = ke^t$. \n",
    "   \n",
    "Sympy is capable of much more than the above, but these are some of its most basic features."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin -- numbers and data types in Python\n",
    "\n",
    "Simple things like numbers have multiple representations in Python.  \n",
    "\n",
    "For example, in your second quiz you wrote an algorithm that divides an integer by 2.  Let's look at various ways Python can deal with this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.5\n",
      "<class 'float'>\n"
     ]
    }
   ],
   "source": [
    "print(3/2)\n",
    "print(type(3/2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3/2 in Python\n",
    "\n",
    "In Python, the **division** operation is implemented in a way that closely mimics how we use it in practice.  But there are also compromises to ensure it isn't an *expensive* computation to do in large quantities.\n",
    "\n",
    "Given two integers $m$ and $n$, Python implements $m/n$ as a **float**. **float** (short for floating-point) data types are an imprecise data type and this can lead to problems.  We will talk more about this later.   \n",
    "\n",
    "What if you want **integer division**?  By this I am referring to the <a href=\"https://en.wikipedia.org/wiki/Division_algorithm\">**division algorithm**</a>. This states that given any integer $n$, and any positive integer $d$ (the **denominator**), there are unique integers $q$ (the **quotient**) and $r$ the **remainder** so that\n",
    "\n",
    "$$ n = q\\cdot d + r \\ \\ \\ 0 \\leq r < d. $$\n",
    "\n",
    "Another way to say this is\n",
    "\n",
    "$$ \\frac{n}{d} = q + \\frac{r}{d}$$\n",
    "\n",
    "i.e. the division algorithm is a procedure to write an arbitrary fraction as a **reduced fraction**, where this means $0 \\leq \\frac{r}{d} < 1$.\n",
    "\n",
    "For example $n=3, d = 2$\n",
    "\n",
    "$$ 3 = 1 \\cdot 2 + 1 $$\n",
    "\n",
    "For $n=13, d = 3$\n",
    "\n",
    "$$ 13 = 4 \\cdot 3 + 1 $$\n",
    "\n",
    "The Python operations for the division algorithm are $//$ and $\\%$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "5//2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "5%2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### isinstance and type\n",
    "\n",
    "Often times the simplest way to figure out how Python interprets your expression is via the **type** and **isinstance** commands. \n",
    "\n",
    "The **type** gives you the name of the underlying Python data type of an object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "isinstance(5, int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**isinstance** allows you to check if an object is of a specific type. This is a useful command to protect a function against mis-use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Changing gears -- basic Sympy examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x\n",
      "x**2\n",
      " 2\n",
      "x \n"
     ]
    }
   ],
   "source": [
    "import sympy as sp\n",
    "x = sp.Symbol('x')\n",
    "\n",
    "print(x)\n",
    "print(x**2)\n",
    "sp.pprint(x**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = x**3 + sp.sin(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sympy types\n",
    "\n",
    "Sympy types at first appear a little confusing.  We will discuss how to interpret them next week.  Even *the same* mathematical object can have different sympy types!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, Sympy's notion of equality is perhaps not what one might expect.  We will also discuss this in more detail next week."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "F = sp.lambdify(x, f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "fresnels(x)"
      ]
     },
     "execution_count": 39,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "F(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sympy algebraic expressions like $\\sin(x)+x^2$ are not callable functions, so one might imagine **what is the point of using Sympy** if you can't use algebraic expressions for their purpose?\n",
    "\n",
    "Sympy allows us to convert their Sympy algebraic expressions into callable functions, with the lambdify command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "fp = sp.diff(f,x )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   2         \n",
      "3⋅x  + cos(x)\n"
     ]
    }
   ],
   "source": [
    "sp.pprint(fp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " 4         \n",
      "x          \n",
      "── - cos(x)\n",
      "4          \n"
     ]
    }
   ],
   "source": [
    "fi = sp.integrate(f, x)\n",
    "sp.pprint(fi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   ⎛ 2⎞\n",
      "sin⎝x ⎠\n",
      "                ⎛√2⋅x⎞       \n",
      "3⋅√2⋅√π⋅fresnels⎜────⎟⋅Γ(3/4)\n",
      "                ⎝ √π ⎠       \n",
      "─────────────────────────────\n",
      "           8⋅Γ(7/4)          \n"
     ]
    }
   ],
   "source": [
    "f = sp.sin(x**2)\n",
    "F = sp.integrate(f, x)\n",
    "sp.pprint(f)\n",
    "sp.pprint(F)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "mpf('0.00052358954761221065')"
      ]
     },
     "execution_count": 45,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "F = sp.lambdify(x, sp.fresnels(x), 'mpmath')\n",
    "F(0.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[<matplotlib.lines.Line2D at 0x7f54dd70b908>]"
      ]
     },
     "execution_count": 47,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX8AAAD8CAYAAACfF6SlAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJzt3Xl8XGd96P/Pd7RrRvuMZFm2rMXybsdxnJAQDNmbQJp0\nL3BLw21LbmkJUOjl0nIvv9v2/loK93a5hf5KCBQoLSEEKCmErCzZg504TmzFtqSRZO2aGa0z2jXP\n748zZzSjGS22JI+k+b5fr7w8c+bMOY9k5/s85/tsYoxBKaVUenGkugBKKaUuPw3+SimVhjT4K6VU\nGtLgr5RSaUiDv1JKpSEN/koplYY0+CulVBrS4K+UUmlIg79SSqWhzFQXYCFut9vU1NSkuhhKKbWh\nvPLKK35jjGep89Zt8K+pqeHEiROpLoZSSm0oItK+nPM07aOUUmlIg79SSqUhDf5KKZWGNPgrpVQa\n0uCvlFJpSIO/UkqlIQ3+SimVhjT4K7VOjU5M8+XnWnmxJZDqoqhNaN1O8lIqnRlj+MN/O8kz532I\nwMO/fx1X7ShNdbHUJqItf6XWoZ+d9/HMeR9/fNsuthTm8lePnk11kdQmo8FfqXXoGy9dwO3K4d63\n1/P+t9Zwon2QFl8w1cVSm4gGf6XWmYnpWZ5t8nHnoUqyMx380pVVADx2ujfFJVObiQZ/pdaZF70B\nJmfC3LSnHICKwlz2VhbyXJM/xSVTm4kGf6XWmZ+e7ScvK4Nrauc6eI81uDnRPsDE9GwKS6Y2Ew3+\nSq0zP28b5GhNCblZGdFjV24vZnrW8GbPSApLpjYTDf5KrSNjUzOc6x3hyu3FcccPRd6/3jmcimKp\nTUiDv1LryOudw4QNHK6OD/5bi3Jxu7I51TmUopKpzUaDv1LryGsdVnA/vL0k7riIcGhbsbb81arR\n4K/UOvJG1zDbSvIodWYnfLZ/ayFeX5DJGe30VSunwV+pdeRc7yh7thQm/WxnuYuwgVZ/6JKu/WJL\ngD/8t1c53aVPD2qVgr+I3C4i50SkWUQ+ucA5vyEijSJyRkT+bTXuq9RmMjkzS6s/xJ4tBUk/byi3\njjf3X/xM38mZWT76rZP88PUePvLgSYwxKyqr2vhWHPxFJAP4AnAHsA94j4jsm3dOA/AnwPXGmP3A\nR1d6X6U2m5b+ELNhw+4Fgn+dx4kINPVdfPB/7HQvfSOT3HXFVlp8Id7Q1n/aW42W/zVAszHGa4yZ\nAh4E7p53zgeALxhjBgGMMf2rcF+lNpVzfdYY/oVa/rlZGVSX5l9Sy/+Z835K8rP4n3ftJ8MhPH5G\nl4pId6sR/KuAjpj3nZFjsXYBu0TkeRF5SURuX4X7KrWpnO0dJStDqHE7FzynodxFU//oRV3XGMPz\nzX7eutNNqTObA1VFHG8bXNZ3w2HD//pBI//vDxsJhzVVtJlcrg7fTKABuAF4D/AlESmef5KI3Csi\nJ0TkhM/nu0xFU2p9ONc7Sr3HRVbGwv9b1pe7aPWHmJkNL/u6vtFJekcmuKraGj56eFsRb3QOL+sa\nTzT28sBzrXzp2VYe06eFTWU1gn8XsD3m/bbIsVidwCPGmGljTCtwHqsyiGOMud8Yc9QYc9Tj8axC\n0ZTaOJr7g+yqSJ7ysTWUFzA9a2gfGFv2dRsjS0Ls22qNIjpcXcz49CzNy1gi+keneynOz6I4P2tZ\nqaJ/fbmdrz7fqh3KG8BqBP/jQIOI1IpINvBu4JF55/w7VqsfEXFjpYG8q3BvpTaFielZuobGqfMs\nnPIBop+3+pY/3NMO/nsrreBvDyU9v0THsTGGn57zcfOeCm7aU87PzvsWTf009Y3yqe+d5n/+RyMv\ntw4set2zvSOaRkqxFQd/Y8wM8CHgceBN4CFjzBkR+XMRuSty2uNAQEQagZ8A/9UYoxuTKhVxYWAM\nY6B2kXw/QF3kc69/+Z2+jd0jbCvJoygvC7Du4ZClh4y2B8YYHp/m6poSrqkpZWhsmo7BhZ84vv9a\nd/T1U419C573lefbuP3vnuUvH31zwXN8o5P0Dk8sWj61Mquyh68x5lHg0XnHPh3z2gAfi/ynlJrH\nnri1VPAvzs+mJD/roiZ6vdkzEm31Q+yoocU7jk93W8NBD1QVYWdxznSPsKMseRmPtw1wpLqYvOwM\nnmteeO+B77zSCcC3jnfwidv3kJ0Z3wadmJ7l7s8/x9D4ND/++A1sKcpNuMb3X+uipszJFdsTug7V\nMukMX6XWATuYLzbSx1bnceFdZtpnZjZMe2CMneWuuOM7ywuWnC9wpnuErAxhV0UBDRUuMhzCme6F\n5we0+EI0lBdwTU0ZZ3tHGZuaSTinf2SCxp4RDlQVMjo5w9nexCWqn2vy0z08wdjULD94vTvh85e8\nAT7y4Gv8+j+9yNRMfKe1MYaPP3SKD3/zZNK00gstfjouor9kM9Pgr9Q60OoL4XZlU5ibteS5tW4n\n3mW2/LuGxpkJG2rntdZ3lrtoC4SYXmTET0t/kB1lTrIzHeRmZVBTtvAcg+GxafzBSerLndFJaskq\nF3vhug8cqwPgdFdi8H+9axiHQFVxXtK+A3s7y6nZMCfa4j9v7BnhO6928sipbp5viX/6eKHZz3u/\n9DK/89XjCdf8yIMnuevzzzE+Fb9u0mOne/nHnzYndGCf7hpOmpbaSJvtaPBXah1oDYSWTPnY6jxO\nfKOTjE5ML3luW8Bq5c5/othZ7mJ61nBhkVZwWyBETUylUet20uZPfn5LpA+izu2KBv9zfYlpJXuE\n0U17yinKy0o607jFF2R7aT5XVhcnfTJ49cJgdCLc/O+/2DLXlfhCS3y34o8ilUZTf5DOmL6LrqFx\nvv9aN693DvP02bm+ionpWX7/G6/w2cfOcaJ9bl5Ex8AYd/7Dc9z1+efiKoUHnvVy+M+f4PWYZbd7\nhyd49/0v8rPzc0PXZ2bD/O/Hz/FGzAqts2HDI6e6GR5b+u90tWjwV2odaPVfRPCPnLecvH+bnU4q\ny487bt+rbYFrhMOG9sAYte6579WUOWkLhJKmU1oiTwT15S6qS/PJyXRwvjdJ8O8LUlmUS0FuFnsr\nCziXJLi39Aep97jYW1lIx8B4XCVnjMHrC/GW2lK2FuVypjv++019QdyubPZsKeDsvF3PTnUORTu9\nY5fGjn16iD1uP6UAPB/Th/H0m1YF0T86SUvMcNkHnm1lYjoc7dMAePD4BV7yDvCXP5zr3H6isY/P\n/6SZe//lRPTYI6e6+PA3T/KJ75xK+H2sFQ3+SqXY6MQ0vtFJat2upU/GyvnD8oJ/qz+EMzsDT0FO\n3PHaJSqQnpEJJmfCcU8MtR4nkzNhekcS0x1ef4isDGF7SR4ZDqHO44oLjLZmXzDa/1DvcdHiC8W1\nnmfDhlZ/iHqPk72VkSeImErEF5wkODlDrdvJvq1F0WGsc+UIUud20VBREDeU1a40bt+/JWF9pOb+\nIBkOYc+Wgrg+jaZIhZab5Ygrw9mY13blMzw+Hf29vBZTgZztGY2Wy06x2Vtx9gxPMBKp2H52znoy\nONE2eNnmSGjwVyrF7FRKbCt7MdWl+YhYHaxLaQ+E2FHmRETijpfkZ1GUt/CoIfuJILavwH6d7Gmh\npT9ITZmTzMjs5DpPYr9EOGxo7p8L/nUeF8Pj0wyEpqLndA+NMzkTZme5K7qKaWwlYnd013lcNFS4\naA/Ez3b2+kLUeZzsrnDRNTROcNLqdLYrjX1bC9lekh+3RIbXH2J7SR6HtxdzpnskGny9viB5WRm8\nvcETl8Jq8QU5vL2YDIdE+0C8kTJWFefR0h+MXsO+z/Ssif7eYise+4nJrsQCoam438da0uCvVIq1\nBuxhnstr+edmZbCtJG95aZ/AGDVJKhURaw2htkDyayQbfWS/bk3ynRZfMG6CWr3bScfAWNzGMz0j\n1gieuZa/M/LduevZwbTe42JrcR7ZmY64z2OHxNa6nUzPGrqGxgGr0zkQmqLO42SnXXFEg/Pc9xrK\nXXEB2OuzUm4NFQUMjc1VRvbxPVsKaPOHop25Xl+IvZUF7CjNj17Hvv6t+yoITs7QNzLJdGSk1Q27\nrdUK7CeRpv5RGiK/A6/PWsm1LTDG7gq7sru0/RoulgZ/pVKs1RdCBHaULa/lD1ZF0brERK+Z2TAd\nA2Nxnbax6tzOBWcKt/lD5GQ62FI4N8Z+S2EuOZmOhJa/HeTqPXOVV53H2njmQmCuY9W+V517Lu0D\nc61mmGvl13usoaV1bmc0gIMV/LMzHWwtzpub8Ba5rjem09muWOzKwv6zzuOkzjPXdxEOWy3yOo8r\noS/F67cqtPrIJjoXBsYYDE0RCE1R73Gxs9wV7cBu8QXJdAg37ikHrEqsPRBiJmz4hUiq6XzfKFMz\nYdoCY9y0t5xMh9DiC9I9NM7UTJhb9pUn/D7WkgZ/pVKs1R9ka1EeuVkZy/6OHbgXyw/bwzwXmjtQ\n63bSPTyRdHiiPdLH4ZhLFzkcQk2Zk9Z5I34uDIwxEzbzgn9iq95+YrCfRLYW55GT6YhL67T4gpQ6\nsymJbGNZ73HFpY+8viC1Zc5ovwIQ/Tzauvc4qS6zUmPemOCfnelga1EeNW6r76JnZILekQnGp2ep\ndTujZfb6Q0zOzNI5OE6dxxXXPxKtYDxO6jxzaSevL0R1aT57t8ylquynmP1bC9lWkofXH6LVb7X0\n91UWUl2Wj9cXipbx+p3uhN/HWtLgr1SKtQbGlj3Sx1bncRKamqV/dHLh6y4xa9iuFJKlflr9oaTp\nolq3M+GJww669TETyex7xgayNn+I3CwHFQXW00SGQ6w5CzEVREt/KNpqt3/OCzHpI2/MqKi5fotI\n6sVvtb6t0UbxqTG70nA4JK7vIvaJoKo4j6wModUfoj1gLbdR73HOpbv8oWhlVud2Ueex0k6dg+PR\npwRPQQ4FuZlxwb/e47I6t/uD0T6AhvIC6txOvP5gtKW/s9yV8PtYSxr8lUohYwytvmDSQLuYZMF1\nPjs9s1A6yU5zzE/jzIYNHQPjSZ8YaiPBOLaT1S5DbM6/IDeLisKcuEDW5k98mqifNyqoxReMe4Ko\n97iYDRsuBMaYng1zITAWvY+IRCqjuZZ/dWl+dEnsWrcrGli9/lD0e7HB3P68zu0iM8NBdWk+Xt9c\nQK51OynMzaLMmU2bP0SLL0h2hoPtpfnRSqqpP0ib30p7iUj0Z2rqD1JVnIczJ5N6j7UU9/neURwy\n9+TQFhjjfF+QwtxMPK6ci5rAt1Ia/JVKoaGxaUYmZhbMyy9kOcM92wJj1jBPV07Sz+0gOD/YdA+N\nMzUbTpgVDFaFMT1r6Bgcjx5r6Q/iKchJmJ1c53bFLUDXOm/SGFgta7tlH5tPn/vcet3iC9I5GJmt\nHFMp1cW0lO2RPrGftfpDTM1YlYb9vdi+ixZfiPzsDCoKrd9RXSRIt/jin5pqIkG5pd968rD6I6yy\n/eRcP1OzYRoiHbb1HhfN/VbLvz46ssnJ+PQszzT5qS7NJzcrg1q3k6mZMM81+6gvtyoO+0ln/rIV\na0GDv1IpZKdcLjb4VxbmkpvlWDRF0LbAME+bKycTT0FOQst/sXWG6hbopI1N1cydawVmY8xc5/O8\na8Z2DNsVRew6RLF9B94kTxi1bic9wxOMTkzTGghFy2efNzY1y8utAWbCJrpXgt130RYIca53lIaK\ngujvqM7tpC0wxpnuYbaV5FGQO7cSqv2kUF8eSTs5rUX2nojsc2CP4Kkvd9I3MsmZ7hF2euI7t1/r\nGIqORLKfvDoGxqOf17mtJ532BUZhrSYN/kqlkL28wsWM9IHYzteFg0SrP0TtEvsD1Ca5xmJ9BXaQ\ntysdYwwtvlBca90WO46/e2iC6VmTMNM4tmUfmyO3OXMy2VKYS0t/MDq5qiFmwxs72P/knI+pmXDc\n/sd2+e21gGI3ytlZ4aKxe4Q3e0einbT2d6ZmwjzZ2Me+mJVQd1W48I1O4vWHEp5M/EFraGh9eXyg\nB6IT1WIrtEPbigDYX1UUPWbf63C1tUrpYquirhYN/kqlUJt/DBHYXnpxwR/slnXynP/UjNXSrksS\nwGNZLdr40TstviAFOZmUFySmi4rzsyl1Zkdb6YHQFMPj03Et7tjygZVWmhvpE1+e2piW/fm+IDmZ\nDqpK8uLOqS930uK3WulVxXlx6SU7wP/HKWv1T3u3MpgLwv9xqjsyOmju3kd3lNA9PMHQ2HTcctf7\nt1oBeXrWRF8DHN5eEn19ZfXcMtJX1VjHd5Tl48qxVsi/asfcudfWlQHgduVEU0tv32WN+3flZHKs\nwQ3AzXutYZ51bidHqov51vGONd/sZlXW81dKXZr2QCiSwln+ME9bndvF42f6mJoJJ6yJ3zE4RngZ\nm8PUuJ34g9YicXaKo8UXpC6Sg05+X2c0J34+MvN1V0Vi8K93z6WIRiesmbbznxBcdss+ktPft7WQ\nDEf8fes9Lr73ahfjUzPRlnT0s3Jr1dEnG/vIznDEXb+yKJeKwhz6RibZV1kY9zu+uqY0+jo2WO+t\nLCAn08HkTJjrd5ZFjx+pLmZ7aR4T02Guq3NHj7/n6mqeauzjwzfP7UrrduXw1796kNGJmbhK/dv/\n5a009oxwOGYPgn/6rasYCE1FzxMR/uG9R3BmZ8R1jK8FDf5KpVD7wBjVF5nysdW6ndbInMGxhKDa\n6lt8mGfsNcB6AjkYSUc09we5fqd7we/UeZz8+Ky1Fo29eNvuJHsPV5XMzdDtG5lgS2FuwhpD9vXO\n943i9YX49au2JXy+q6KA0ckZRvuC3Hloa9xnOZkZXFNTynPNfq7aURId6QNWIH3Xwa185flW3nWo\nMu57+yoLuW1fBblZGeyPeVrIzHDwjd97C12D43GVQmaGg0c/fIypmTB52XOVSI3bydMfvyGhzL95\ndXXCseqy/IS/a2dOJs6c+DBcVRz/5LNWNPgrlULtgRC37K24pO/WxuTfE4L/MncGq42O+AlycFsR\noxPT9I1MJmz+EquhvICHTnQSCE5yrm+UkvyspEHdnqH7Zs8IXYPj0cplvsPbi/nHn7ZYr6sTd+a6\nKTJrFqzlE+a776ad9AyPc99NOxM++2937OZtDWUca/DEHXc4hPt/+2jS8lxdU8rVNYnHC5ax18JG\nosFfqRQJTs7gD04tuC3iUuaWIwgC8UHR6w9Rkp9FcX72otfYEZkJGztWHhLTM7HsAH3ywhCnOobZ\nt7VwwRTR9TvdfPm5VgB+7Whiqx7gF6/Yyj/+tIXsDAc37U4M7luL8/jCe48wEw7H5edtb6krS9r6\nBuvJ4KY9l1a5bnar0uErIreLyDkRaRaRTy5y3q+KiBGR5FWuUmnEHs53sSN9bNHO1yTDPduWuT9A\nblYGtWVOGiNLE9urS+5KksaxHawqIitDeOrNPhp7RriurmzBc+84sCXmdWXSc/ZWFvLND1zLwx+8\njqL85K3rdx2q5O7DVUv+PGr5VtzyF5EM4AvArUAncFxEHjHGNM47rwD4CPDySu+p1GbQHri0YZ6x\n6haYEer1L563j3XF9mJeiGx5eKrD2vBk/pDMWLlZGRypLuHB4x0AXFe/8H2u2lHCX9y9n1JnzqKV\n0XX1C1cgam2sRsv/GqDZGOM1xkwBDwJ3JznvL4C/BhJ3glAqDbVFW/6XlvYBkq4FMzQ2Rd/I5KKt\n91iHthXRNzJJ19A4r3UMccX24gXTOLb3v7UGsDpOr9yemKe3iQjvu64mocNVpd5qBP8qoCPmfWfk\nWJSIHAG2G2N+uAr3U2pTuBAYw+3Kjo4PvxR1Hhf+4GR0RyiYS93sS5IfT+ZtkSeEr7/QxtneUa6t\nK13iG3DHwUp+cN/b+OYHrl3zIYlqbaz5JC8RcQB/A3x8GefeKyInROSEz+db6nSlNjR7+YWVSLYX\n75uRrQOTdY4m01BRwM5yF198xgvAnQe3LvENy4GqogVz9Gr9W43g3wVsj3m/LXLMVgAcAH4qIm3A\ntcAjyTp9jTH3G2OOGmOOejye+R8rtam0B8ZWlO+H2N2w5mb6NnaP4HblJB1+uZCP3tKAQ+BXjlRd\n8rwDtbGsxlDP40CDiNRiBf13A++1PzTGDAPRHiER+Snwx8aYEyiVpiamZ+kZnrjoBd3mq3E7ycvK\n4FTHML98pTWUsrFnJGEm7FLuPLSVW/ZWkJOpK76kixX/TRtjZoAPAY8DbwIPGWPOiMifi8hdK72+\nUptRxyUu6DZfVoaDIzuKOd42AMDw+DRne0fiZqcuV25WxpIdvWrzWJVJXsaYR4FH5x379ALn3rAa\n91RqI2uLDvNcWcsfrBmp//fpJgLBSU5eGMKYuQXFlFqIPuMplQLt0XX8V55fv23fFsIGHj/Tx49O\n91KQkxm3eJhSyejyDkqlQHtgjMLczCWXX1iOvZXWaJ2/feo8w+PT/OqRbZe0SqhKL9ryVyoF2gKh\npDtlXQoR4VPv2otvdJIsh/CBY7Wrcl21uWnLX6kUaA+MccUqpmZu3F3OUx97O86cTCqLLs+SwGpj\n05a/UpfZ1EyYrqFxdlzC7l2L2VleoIFfLZsGf6UuswsDY8yGTdy2gkpdbhr8lbrM7H13k+17q9Tl\nosFfqcvM3v9WW/4qlTT4K3WZeX1BPAU5FG6ybQHVxqLBX6nLzOsPRbdgVCpVNPgrdZm1+IKa71cp\np8FfqctoIDTF0Nh0dClmpVJFg79Sl5E90qdeW/4qxTT4K3UZtUSHeWrLX6WWBn+lLiOvL0R2hoNt\nJbpblkotDf5KXUYtvhA17nwydNNzlWIa/JW6jFp8Qc33q3VBg79Sl8n41CxtgRC7Ki5uf12l1oIG\nf6Uuk/N9oxjDRW+urtRaWJXgLyK3i8g5EWkWkU8m+fxjItIoIq+LyNMismM17qvURnKudxSA3VsK\nU1wSpVYh+ItIBvAF4A5gH/AeEdk377STwFFjzCHgYeCzK72vUhvN2d5RcrMcVK/yOv5KXYrVaPlf\nAzQbY7zGmCngQeDu2BOMMT8xxoxF3r4EbFuF+yq1oZzrG2F3RYGO9FHrwmoE/yqgI+Z9Z+TYQn4X\n+FGyD0TkXhE5ISInfD7fKhRNqfXjbM8ou7dovl+tD5e1w1dEfgs4Cnwu2efGmPuNMUeNMUc9Hs/l\nLJpSa8o3OkkgNKX5frVurMYG7l3A9pj32yLH4ojILcCngHcYYyZX4b5KbRh2Z+9ebfmrdWI1Wv7H\ngQYRqRWRbODdwCOxJ4jIlcAXgbuMMf2rcE+lNpTT3cMA7K3Ulr9aH1Yc/I0xM8CHgMeBN4GHjDFn\nROTPReSuyGmfA1zAt0XkNRF5ZIHLKbUpvd45RHVpPiXO7FQXRSlgddI+GGMeBR6dd+zTMa9vWY37\nKLVRneoY5nB1caqLoVSUzvBVao0FgpN0DY1zxbaiVBdFqSgN/kqtsdc7rXz/oW3a8lfrhwZ/pdbY\nqc4hROBAlbb81fqhwV+pNXaibZDdFQW4clali02pVaHBX6k1ND0b5pX2Qa6tK0t1UZSKo8FfqTX0\nRtcw49OzXFNbmuqiKBVHg79Sa+hl7wCABn+17mjwV2oNvdwaoN7jxO3KSXVRlIqjwV+pNTIxPctL\n3gBv2+lOdVGUSqDBX6k18kKLn4npMDfvrUh1UZRKoMFfqTXy9Jv95Gdn8JY6zfer9UeDv1JrwBjD\nj8/2c6zBTU5mRqqLo1QCDf5KrYET7YP0DE/wC/u3pLooSiWlwV+pNfDdVzvJy8rQ4K/WLQ3+Sq2y\n0OQMPzjVw+0HtuDUJR3UOqXBX6lV9u0THYxOzvC+63akuihKLUiDv1KraHJmli8928qV1cUcqS5J\ndXGUWpAGf6VW0ddfaKdraJyP3bor1UVRalGrEvxF5HYROScizSLyySSf54jItyKfvywiNatxX6XW\nE68vyN88eZ4bd3s41uBJdXGUWtSKg7+IZABfAO4A9gHvEZF98077XWDQGLMT+Fvgr1d6X6XWk0Bw\nkg98/QQ5WQ7+6lcOpbo4Si1pNVr+1wDNxhivMWYKeBC4e945dwNfi7x+GLhZRGQV7q1USoXDhp+c\n7eeuzz9P19A4X/ytq9hSlJvqYim1pNUYh1YFdMS87wTestA5xpgZERkGygD/KtxfqTU1PRvmuWY/\nP28doH9kkpGJacanZhmbmqEtMMZAaIodZfk8eO91HN6u+/SqjWFdDUIWkXuBewGqq6tTXBql4Lkm\nP3/yvdfpGBgnK0PwuHIozMsiPzuD/OxMbtxdztt3ubnjQCXZmTp+Qm0cqxH8u4DtMe+3RY4lO6dT\nRDKBIiAw/0LGmPuB+wGOHj1qVqFsSl2y753s5GMPnaLO7eSL77uKd+zykJul6/SozWE1gv9xoEFE\narGC/LuB98475xHgHuBF4NeAHxtjNLirdetE2wB//O3Xua6ujAfuOUp+9rp6SFZqxVb8LzqSw/8Q\n8DiQAXzFGHNGRP4cOGGMeQT4MvAvItIMDGBVEEqtS+NTs3z0W69RVZzHF993lQZ+tSmtyr9qY8yj\nwKPzjn065vUE8OurcS+l1tqXn/PSOTjOg/deS0FuVqqLo9Sa0B4qpWKMTkzzTz/zctu+Cq6tK0t1\ncZRaMxr8lYrx8CudBCdn+MMbd6a6KEqtKQ3+SkWEw4avvdDGkepirtDx+mqT0+CvVMRLrQHaAmPc\n89aaVBdFqTWnwV+piEff6CEvK4Pb9unuW2rz0+CvFDAbNjx2uo+b9pSTl60TudTmp8FfKeDnrQP4\ng5O882Blqoui1GWhwV8p4Mdn+8jOcHDjHl2HX6UHDf5KAc82+blqR4nO5lVpQ4O/Snu+0UnO9o7y\ntgZ3qoui1GWjwV+lvRdarG0ljmnwV2lEg79Ke883+ynOz2L/1qJUF0Wpy0aDv0p7J9oHObqjhAyH\n7iyq0ocGf5XWhsam8PpCXFldkuqiKHVZafBXae3khSEAjmjwV2lGg79Ka6+0D5LhEK7Yrvl+lV40\n+Ku09uqFQfZWFuj4fpV2NPirtGWM4Y3OYa7Ypss3q/SjwV+lrc7BcUYnZ3SIp0pLKwr+IlIqIk+K\nSFPkz4ReMxE5LCIvisgZEXldRH5zJfdUarWc6R4GYP/WwhSXRKnLb6Ut/08CTxtjGoCnI+/nGwN+\n2xizH7g9m9GtAAAeHElEQVQd+DsR0edslXKN3SM4BHZvKUh1UZS67FYa/O8GvhZ5/TXgl+afYIw5\nb4xpirzuBvoBXTpRpdyZ7hHqPS5ys3T9fpV+Vhr8K4wxPZHXvUDFYieLyDVANtCywvsqtWKNPSOa\n8lFpa8nxbSLyFJBsX7tPxb4xxhgRMYtcpxL4F+AeY0x4gXPuBe4FqK6uXqpoSl2ygdAUPcMT7NPg\nr9LUksHfGHPLQp+JSJ+IVBpjeiLBvX+B8wqBHwKfMsa8tMi97gfuBzh69OiCFYlSK9XYPQLAvkod\n6aPS00rTPo8A90Re3wN8f/4JIpINfA/4ujHm4RXeT6lVcb5vFNDOXpW+Vhr8PwPcKiJNwC2R94jI\nURF5IHLObwBvB94vIq9F/ju8wvsqtSKt/hAFuZm4XdmpLopSKbGiOe3GmABwc5LjJ4Dfi7z+BvCN\nldxHqdXWFghR63Yioss4q/SkM3xVWmr1h6gpc6a6GEqljAZ/lXYmpmfpGhqn1q3BX6UvDf4q7XQM\njGEMGvxVWtPgr9JOqz8EQI0Gf5XGNPirtGMH/1rN+as0psFfpZ22QIhSZzZF+VmpLopSKaPBX6Ud\nry+k+X6V9jT4q7TTFtBhnkpp8FdpJTQ5Q9/IJLXu/FQXRamU0uCv0kpbINLZ63aluCRKpZYGf5VW\n2vxjANRoy1+lOQ3+Kq3YLX/N+at0p8FfpRWvL0RFYQ7OnBWtaajUhqfBX61boxPTzIZXd08fHemj\nlEWDv1qXfnK2n6v+4ine+ffPEpycWbXrtvpD1Hk0+CulwV+tO8YY/tcPG8nNcnCub5RvvnxhVa47\nPD7NQGhKW/5KocFfrUMnO4Zo8YX41Lv2cnVNCd88vjrBv00XdFMqSoO/Wnd+erYfh8DtByp518FK\nvL4QFwJjK76uvaBbnQZ/pTT4q/XnRW+Ag1VFFOVlcWyXB4Bnm30rvm6rP4QIbC/VMf5KrSj4i0ip\niDwpIk2RP0sWObdQRDpF5PMruafa3GZmw5zqGObqmlLAaqW7XdmcvDC04mu3BUJUFeeRm5Wx4msp\ntdGttOX/SeBpY0wD8HTk/UL+AnhmhfdTG8TE9Cy//ZWf8+v/9ALD49PL/l7H4DhTs2F2bSkAQEQ4\nUFXE6a7hFZep1a+reSplW2nwvxv4WuT114BfSnaSiFwFVABPrPB+aoP4zqudPHPex/G2Qb7xUvuy\nv+f1BQGojxmOebCqiKb+IBPTs5dcHmOMbtquVIyVBv8KY0xP5HUvVoCPIyIO4P8Af7zUxUTkXhE5\nISInfL6V53jV6gpNzjA5s7wA/PiZPuo8To5UF/PoGz1LfyEiustWzMJrB6qKmA0bGntGLq7AMQKh\nKUYnZrTlr1TEksFfRJ4SkdNJ/rs79jxjjAGSTcf8A+BRY0znUvcyxtxvjDlqjDnq8XiW/UOotdc/\nOsE7PvcTbvvbZ5acdDU1E+Z46wDHdrq5bf8WznSP0D86saz7tPhCFOdnUerMjh47UFUEwJnuSw/+\nbdFKRYO/UgBLLnBijLlloc9EpE9EKo0xPSJSCfQnOe064JiI/AHgArJFJGiMWax/QF0mxhgeO93L\n3srCRce/f/tEJ/7gFP7gFI+81s1731K94Lmnu4cZn57luvoy3K4cAF7vGOaWfblLlsfrCyYMxdxa\nlIszO4OW/uAyf6pEumm7UvFWmvZ5BLgn8voe4PvzTzDG/CdjTLUxpgYr9fN1DfyXz7neUUYnFu5w\n/f5r3XzwX1/lPz3w8qLr6Dzb5GNfZSFVxXk8t8Swy8ZIC33/1iL2by0iwyGc6lzeaB2vP0SdJ36t\nfRGhvtxFi29lwT/TIWwrybvkayi1maw0+H8GuFVEmoBbIu8RkaMi8sBKC6cWFw4bHj/Tu2BK5Xjb\nAL/wd8/wvi//HCsrl+g7r1rZuK6hcV69MJj0nInpWV5tH+L6nWUcrSnhlfbBBa8HcLZ3hILcTLaV\n5JGXnUFDuWtZKZvRiWl8o5NJ196pczvx+kJLXmMhbYEQ20vzycrQqS1KwQqDvzEmYIy52RjTYIy5\nxRgzEDl+whjze0nO/6ox5kMruedmNDE9S99I8gD+3Vc7+cTDp5KOdPm3n1/gv/zLK/zRt15L+t1v\nn+gA4LWOIc73Jbaaw2HDq+2D3HmoEoATbcmDf6s/xNRsmEPbirlqRwl9I5N0Dy+cwz/bM8reLYWI\nCAA7l9lqt4N7XZJdtuo9LrqGxhmburRF3lr9Y5rvVyqGNoNWIFnrNxw2/PhsH+NT8cF6ZGKaD/3b\nqzz9Zl/Cd+775kmu/8yPo+kS2/RsmI89dIqHTnTyH6e6E75nH3uhJcBAaCrh88aeEXaWW4H0JW8g\n4XOvP0RoapZ37PJQ53bySvtA0p8zGpQ9TvZWFgJwvm806bkALb4g9eVzAbze46JjYGzJoZpef+Iw\nz+g1Ite7lNa/MYY2HeapVJxNGfyHxqYSAvP8wBMOG05eGCQck+eemQ3z9Rfb6Boajzv3iz9r4S9+\n0Bh3zbO9I+z79ON86Rlv3LlfetbL73z1BH/z5Lm44w8d7+AHr/fwiYdfj7vOQGiKJxv7mAkb/v21\nrrjvnOqYy5O/OC94h8OG1zuH2bOlAGNImAQVDhta+kMca3BTkp/Fm0mGSb7RZV3/0LZiDm4r4s2e\n5AG9NRKUa91Odkby8Qt1vg6PTTM4Nh23QfrOchdhM9fpuhCvL4RDoLoscfmFevu+l5D37xuZZHx6\nVjdtVyrGpgv+rf4QN/2fn/HwK3MjS58408uB/+dxHjvdGz32ledb+eV/fIGvvtAWPfYfr3fz6e+f\n4cPfPBk95g9O8lc/OsuXn2uNG2f+0PFOxqdn+b8/boq7/48i93iyMb6F/3yzH7DGm18YmFuk7I2Y\noP36vE7Rl1utlviR6uKE4N47MsH49Cy/eMVWgIQx8N3D44xPz7Kz3MW+rYVJx8g39QXJdAj1Huei\naRWvP0RlUS752ZmUOLMpdWYvGITtbRJ3xLSylxu4vT4rL5+Tmbj8wo6yfBxiDQW9WN5o5aWbtitl\n23TBf0dpPlXFeXzl+bboscfO9DITNtEcOMATkeD8+Jm5CuHVdiv4vtE5zMxsGIjPg7/knUuLvBLp\nHB2dmME3OglYaRo7ddMWGGNobC4V80bXcDQFc653roVtz2i948AWznSNxD2JNPWNUlWcx7EGD839\nQUIx4+vtQHpldTFVxXkJHap2kNzpcbF3SyHnekcTRvO0BUJUl+aTmeGIdrIma517ffHLIuz0uGjp\nTx6E7eAfe36dx4kINC8xVLMlyTBPW25WBttK8qO/r4uhm7YrlWjTBX+HQ7h5bzlne0eiwdJOUbwW\nSaOEw4Y3Oq2W9Nne0WgapqnfCspTs2HaIksI20E2O9NBc+RzYwze/iC7KlyRa1iBt2NgjKnZMO86\nWBn33eHxafzBKd55YAsQny9v8QUpzM3k7bs8jE7O0Dk4l3JqDYxR485nb2UhYRPfcvbGBPc9Wwpo\nmpeDtwNtfbmLhgoXkzNhugbj01leXyg67t3uZJ2fUzfGWGPvY/Lw9eXOhVv+/jFEoDpm5UwrcOct\nmq8Phw1tgcRhnrFq3M5o5XIx2gIhsjMdbC3SYZ5K2TZd8AfYs6UQY6wAGA4bmiKBMBCaIhCcjKZM\nGspd0cAM0NwfinZo2sG0xRdkS2Euh7cVRwOqLzjJ6OQMdxywgrydT7cD0637rFUu7NaxPbv0QFUR\n20vzOBvX8rcCnv1U0BJJURhjaPUFrTx75LPYlrPXF8SVk4mnIIf6chet/lBcy77FF6Q4P4syZ3bS\ntEs4bGgPzI2AqXVbrfP5AXogNMXIxExcyqTe4yIQmmIwSSdzWyDE1qLElTNr3a5FA3f38DgT0+FF\nt1isLcunzT+26DDTZLy+EDVl+TgcclHfU2oz25TB326Rn+8bjeSxZ3nnQavV3dQfjKY2bttfETk2\nytDYFP7gJLfv34II0aGRXl+I+nIn9eWuaPBtjQTIq3aU4HZlR4O8HTiPNbitJ4VIsG2NWVpgp8cV\nl7e2gr8zmu6wrzE4Nh0NujvK8sl0SFzwbol8T0SoczuZnAnTHdNR3dwfpN7jsiZIJQn+faNWBWi3\n/POyM9halJfQoo9ugOKJT+MAtCYJ5q3+UNL0Sm1ZPq2+0IKBe7FhnrYat5Pg5Ey0sl4u3bRdqUSb\nMvjvKHOSnemgqT8YTeW8M5KKaeoPRvPGt+2LVAh9wWhgP7StiKpiKwgaYyJ5aKtlPjg2TSA4GRfM\na8qc0SDY6rfWpSlz5UQmJc0Ff4mMYqnzuGj1W08kwckZekcmqPe4KHVmU5SXFfMd6886t5OsDAfV\nZflxeXavLxgN6nVJgrvXF4yOzClxZlM2r5M22a5WdZ7EtMpcUJ47z34KaE2SxmkPhOI6e+e+42R0\nkcBtlyfZMM/Ya8SeuxyzYcOFwBi1umm7UnE2ZfDPcFit3fN9ozRFWvDHdnooyMmkqW8Urz9EXlYG\nB6uKcOVk4vXNBf+d5S7qPC68/qCV3pmYod7jjLZ2vf4Qrf5IDrk4z8pD++eCf2waxRtzvKo4j5zM\nDOo8Tiamw/SMTESDZ73dgvfMzWK1/7Rb5tYTg1XGsakZuocnogHZDpj2E4X1FDMVTRdZ58R30iZb\n66bW7UxonXv9IbIyhG0lc635bSV5ZDokIQhHh3kmC/6RimihwB2bxlqI/bttu4jg3z1k7Q+QrExK\npbNNGfzBSv009QU53xekojCHovwsdkaO2akWR2SYY4svRFN/kNwsB1XFedRFgqAdLOvLXdRHO0SD\ntERyyBkOodbtpH90ktDkjBX8y+aC/4XAGDOzYdoC8ZUCWK1mewii3XKvc7uix9oC8WvR1JdbOfOZ\n2XBMKsb63vynhrnO3oU7adv8IXIyHVQWzi22VlNmtc4DMbl8ry/IjjInGTH58qwMB9Wl+QmB3H4C\nSrZ4Wl201Z68o9ha08cZnRWcTFVxpNK5iE5fXdBNqeQ2cfAvoGtonJMdg+yqsHaF2lVeQFN/0JqB\nGgmc9oJhdo7crhBCU7O80GKNzd9Z7qKqJI/sTActvhDN/aPRVrWdSz7TPULP8ER0Jmqt28lM2NAx\nOE5Lf8z9In96/VYl4hBrDDtYaZe+kUmCkYqkOmYtmnqPi+lZw4WBsWgL3w7u858a7CC/01MQ/X3M\n76S1n1JiO0GTtawX2v0q9snGZn+vJskkra3FeWRnOGj1J9+I3esLLbmxemak0rmYlr9u2q5Ucps2\n+DfELAfQUG4FwYYKF/7gJJ2D49E0Tr3HRc/wBKc6h6IB3W5RP3GmD1dOJlsKc61WfpmTxu4RLgyM\nRa9pd24+FVm2oSF6Dev6L7T4CU3NRq9dXpCDMzsDry+E1xdkW8ncpKa6mKeCpr55wytjUjteXxAR\n4jox62PSQs39QXIyHVTFrGAZW+lYfyZ2gs7Pqc9GRgQlG4FTG0l3xc5L8PqCC87QzXAI1WX5SVv+\n41OzdA2NLzrM01bjdl5Uzr/VH8KZnbFoOkmpdLRpg7/d2gfYt9UavhmbA7df20FxaGya3ZF9Y+1g\nd65vNJqPt48/1+wnbKyKBOaGSNq7VTVE7muPWnniTF/c/USEWo/Vaj7bOxodmWRd3xW9b6s/FPcz\n2E8ULT4rbTV/I/I6j5V+Gp2YpiUyKSs2VWP/nM39QWZmw3QMJHaCzs/ld0X2003Waq71OBmfnqUv\nZkVRr3/hGbr27ypZ4E42omghNWVO2gPLH+7ZFumAXiydpFQ62rTBf3vMJKO31JYCRIM7wNU11rH9\nkYoB4MrtJQBsKbQ2DwHYH9lFav7390Re52dnUut20jk4Tk6mg+2R1rY9wuZn562172Mrnjq3izc6\nh2jxBTm0rTh6fEdZPiLwVGStn9j7FeZmUV6QQ0t/kMaeEXbHVAwQ07L3hTjXOxpXcQBxaauuoXGm\nZ01COiczw8H20vzoiJ9m32jctWPF9l3Y5s8Enq/O7aQtMBb3tABzTyOLDfOcu2++VemMTC55brRM\nOtJHqQSbNvhnOIR/fv/VfPZXD0UrgsqiPD54Qz1/dMsuKiIdndtL8/nFK7ZyeHsx10QqCRHhl66s\nAogudwzwjl3W1pLZGY64gHhdXRkAR2tKyIxZL/7IDqsyqS7Nj+5oBdaTyODYNMbAwW1zlUtuVgbV\npfk8FllyYn4Ar/e4OJWk0rA+swLcifZBuobGOVBVGPd5hsOaD9DSH4yOgEo2rNJqnVt5eXuuQ8O8\nckDMjOBIqz0ctjZIX2qc/tRMmO7hxJnG9r2XEh1muozUz8T0LJ2DY0krL6XS3ZLbOG5kN+4pTzj2\n327fk3DsH95zZcKxP7trP7/7ttq4PPSV1SX8/bsPs6uiIC6N8IFjdbQHxvijW3fFXeO911Tz1Jt9\n/ObV2+PLtbucz/zoLABHIk8btqtrSmkPjOHKyUwI/jvLXbz4krW656HtRXGfVZdaaZ5HIiuDHtga\n/zlYlceZ7mFe7xzCIURnM8eqczt5ocXPzGyY832j1kipvKyE8yoKc8jLyogGYXvW9GKt7Ng+hdih\noy2+IFXF1sYvS7H7WNoCIa6rL1v03PbAGGGz+NwBpdLVpg7+K2EtdpbYYrz7cFXCsRq3k2/83lsS\njt+4p5xX//utCcFz95YCPvtrh9halEdRfvxnv33dDp5s7ON3rq+Ny9mDNXP4X15qB+DojvhKIzvT\nwb7KQk51DpPhkLh0la3e4+RHp3s43maNgMrPTvzrP1BVxMR0mKb+IGd7EtNHNhGJ63yNDi9dIu0D\nVvA/1uCJHj/fF4z2oSxla5GVvlrOiB976Ku2/JVKpMF/jZU4s5Me/42j25MeP7StmJP/49ak69Dc\nsreC+27ayf6thRTkJrbG77piK290DXPDLk/S1vqBqiLCxtobYKEN2A9F0lDPN/s52zvCfTc1LPiz\n1bmdnOm2Fsg7Hflz39bEpwmbJ2akk21mNkyLL8ixBveC34vlcAg7kswxSMYe/aQ7eCmVaEXBX0RK\ngW8BNUAb8BvGmIS9AEWkGngA2A4Y4J3GmLaV3HszW2gBModD+Phtuxf83u++rZZtJXnRvov5jjV4\nKMjNZHRihl++MvEJBqzRNMX5WXz+J82EDVxbt3BqZd/WQn74Rg/DY9Oc6Rphe2kexfnJKztIfFoA\naB8YY2omHB0iuxzLHe7p9Vn7EDhztI2j1Hwr7fD9JPC0MaYBeDryPpmvA58zxuwFrgH6V3hflYTD\nIdxxsJIyV/Ix7XnZGfzgvrfxjd99S3S0U7Jr3Lq3gqGxaQpzM7myujjpeQBHqq3U06sXBjl5YZCD\nSVJN89XOW5bZXj01dmTTcq7RPpA4ami+Fn9IUz5KLWClwf9u4GuR118Dfmn+CSKyD8g0xjwJYIwJ\nGmOST/NUa25HmZO3LZFi+eitu7hht4e//JWDCUszx7piexGZDuGLz7TQPTzB22Py+Aupi+zna+9x\nfK53bk2l5aopSz5qKJa958Jy5g4olY5WGvwrjDE9kde9QEWSc3YBQyLyXRE5KSKfE5Glh3WolKkq\nzuOr//ka7jy0ddHz8rMzuf3AFl7yDiACN+9N9tcf72Ck38HuI7BnVifrfF5IdMTPAktFwNyeC7qs\ng1LJLRn8ReQpETmd5L+7Y88z1pTLZM/hmcAx4I+Bq4E64P0L3OteETkhIid8Pt/F/iwqBe67qYG9\nlYV8/NZdy1pC4fB2K4302oUhwmHDqxcGuaq6ZIlvxbPnEiy2wFt0KWpN+yiV1JLNLWPMLQt9JiJ9\nIlJpjOkRkUqS5/I7gdeMMd7Id/4duBb4cpJ73Q/cD3D06NGL265JpcTuLQX86CPHln2+pyCH6tJ8\nXvQGeFuDm6Gxaa7acXHB355jsNhwz7OR3dUupi9BqXSy0rTPI8A9kdf3AN9Pcs5xoFhE7ITwTUDj\nCu+rNrBb91XwXJOfh0504JDkk/EWIyLsKFt8dc83e0YpdWZTrgu6KZXUSoP/Z4BbRaQJuCXyHhE5\nKiIPABhjZrFSPk+LyBuAAF9a4X3VBnbnoUqmZsP88/NtXL/TfUkrbta6nYumfd7sHWFvZYEu6KbU\nAlY0ANoYEwBuTnL8BPB7Me+fBA6t5F5q87iyuoQ/uKGe55r9fPrOfZd0jVq3kycb+5iaCZOdGd+G\nmZkNc7Z3lHuu27EaxVVqU9LZLyolPnH7Hj6xgu/vrSxkJmxo6h9l/7x1jFr9IaZmwknXLlJKWTbt\nqp5qc7OX4j7TPZLwWWOks1eDv1IL0+CvNqSaMif52Rk0Jgn+Z7pHEpbdVkrF0+CvNiSHQ9hbWRhd\nWC7WibYBDm0rSugLUErN0f871IZ1sKqI010jTM2Eo8cmpmd5o2uYowusXaSUsmjwVxvWtXWljE/P\n8nrnUPTYqY4hpmcNV9dc3MQxpdKNBn+1Yb2ltgwReLElED32fEsAh3DRs4aVSjca/NWGVeLMZs+W\nQp5r9kePPdXYx1U7ShbdV0AppcFfbXC37i3neNsAvcMTNPWN0tgzwm37tqS6WEqtexr81Yb2y0e2\nETbwz8+38v/9rIXsTAe/ciT5LmVKqTk6w1dtaLVuJ7921Ta++IwXgA/eUL/gTmZKqTka/NWG92d3\n7afUmU1uVgYfunFnqouj1IagwV9teM6cTP70nXtTXQylNhTN+SulVBrS4K+UUmlIg79SSqUhDf5K\nKZWGNPgrpVQa0uCvlFJpSIO/UkqlIQ3+SimVhsQYk+oyJCUiPqB9BZdwA/4lz7r8tFwXR8t1cbRc\nF2czlmuHMcaz1EnrNvivlIicMMYcTXU55tNyXRwt18XRcl2cdC6Xpn2UUioNafBXSqk0tJmD//2p\nLsACtFwXR8t1cbRcFydty7Vpc/5KKaUWtplb/koppRawaYO/iBwWkZdE5DUROSEi16S6TDYRuU9E\nzorIGRH5bKrLM5+IfFxEjIi4U10WABH5XOT39bqIfE9EilNYlttF5JyINIvIJ1NVjlgisl1EfiIi\njZF/Ux9JdZliiUiGiJwUkR+kuiyxRKRYRB6O/Nt6U0SuS3WZAETkjyJ/j6dF5JsikrsW99m0wR/4\nLPBnxpjDwKcj71NORG4E7gauMMbsB/53iosUR0S2A7cBF1JdlhhPAgeMMYeA88CfpKIQIpIBfAG4\nA9gHvEdE9qWiLPPMAB83xuwDrgX+cJ2Uy/YR4M1UFyKJvwceM8bsAa5gHZRRRKqADwNHjTEHgAzg\n3Wtxr80c/A1QGHldBHSnsCyxPgh8xhgzCWCM6U9xeeb7W+ATWL+/dcEY84QxZiby9iVgW4qKcg3Q\nbIzxGmOmgAexKvKUMsb0GGNejbwexQpi62IXexHZBrwLeCDVZYklIkXA24EvAxhjpowxQ6ktVVQm\nkCcimUA+axS7NnPw/yjwORHpwGpdp6S1mMQu4JiIvCwiPxORq1NdIJuI3A10GWNOpbosi/gd4Ecp\nuncV0BHzvpN1EmRtIlIDXAm8nNqSRP0dVmMinOqCzFML+IB/jqSkHhARZ6oLZYzpwopXF4AeYNgY\n88Ra3GtD7+ErIk8BW5J89CngZuCPjDHfEZHfwKrhb1kH5coESrEez68GHhKROnOZhl0tUbY/xUr5\nXHaLlcsY8/3IOZ/CSnH86+Us20YhIi7gO8BHjTEj66A8dwL9xphXROSGVJdnnkzgCHCfMeZlEfl7\n4JPA/0hloUSkBOtpshYYAr4tIr9ljPnGat9rQwd/Y8yCwVxEvo6VawT4NpfxsXOJcn0Q+G4k2P9c\nRMJY63j4Ulk2ETmI9Q/ulIiAlVp5VUSuMcb0pqpcMeV7P3AncPPlqiiT6AK2x7zfFjmWciKShRX4\n/9UY891UlyfieuAuEXknkAsUisg3jDG/leJygfXU1mmMsZ+QHsYK/ql2C9BqjPEBiMh3gbcCqx78\nN3Papxt4R+T1TUBTCssS69+BGwFEZBeQzTpYWMoY84YxptwYU2OMqcH6n+PI5Qj8SxGR27FSB3cZ\nY8ZSWJTjQIOI1IpINlZH3CMpLA8AYtXWXwbeNMb8TarLYzPG/IkxZlvk39O7gR+vk8BP5N91h4js\njhy6GWhMYZFsF4BrRSQ/8vd6M2vUEb2hW/5L+ADw95FOkwng3hSXx/YV4CsichqYAu5JYUt2o/g8\nkAM8GXkqeckY8/uXuxDGmBkR+RDwONYojK8YY85c7nIkcT3wPuANEXktcuxPjTGPprBMG8F9wL9G\nKnIv8J9TXB4iKaiHgVexUpwnWaPZvjrDVyml0tBmTvsopZRagAZ/pZRKQxr8lVIqDWnwV0qpNKTB\nXyml0pAGf6WUSkMa/JVSKg1p8FdKqTT0/wPyYtRNS+filAAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f54df861a90>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import mpmath as mp\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "%matplotlib inline \n",
    "\n",
    "dom = np.linspace(-8, 8 , 1000)\n",
    "plt.plot(dom, [F(i) for i in dom])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advantage of Sympy algebraic expressions\n",
    "\n",
    "Sympy allows for many basic operations on algebraic expressions, like differentiation and integration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " ⎛   2⎞\n",
      " ⎜ -x ⎟\n",
      " ⎝ℯ   ⎠\n",
      "ℯ      \n",
      "⌠           \n",
      "⎮  ⎛   2⎞   \n",
      "⎮  ⎜ -x ⎟   \n",
      "⎮  ⎝ℯ   ⎠   \n",
      "⎮ ℯ       dx\n",
      "⌡           \n"
     ]
    }
   ],
   "source": [
    "f = sp.exp(sp.exp(-x**2))\n",
    "sp.pprint(f)\n",
    "F = sp.integrate(f, x)\n",
    "sp.pprint(F)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Asking sympy to solve equations\n",
    "\n",
    "Sympy has some fairly sophisticated algorithms to solve polynomial equations. It uses these algorithms to build tools to solve (symbolically) a wide array of equations, even ones that are not polynomial. \n",
    "\n",
    "Sympy can:\n",
    "\n",
    " * Factor polynomials.\n",
    " * Find roots of polynomials, symbolically as well as numerically. \n",
    " * Solve (symbolically as well as numerically) simultaneous polynomial equations.\n",
    " * Solve simultaneous equations that are not polynomial\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-√2, √2]\n"
     ]
    }
   ],
   "source": [
    "p = x**2 - 2\n",
    "sp.pprint(sp.solve(p, x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   2          \n",
      "a⋅x  + b⋅x + c\n",
      "\n",
      "⎡        _____________   ⎛       _____________⎞ ⎤\n",
      "⎢       ╱           2    ⎜      ╱           2 ⎟ ⎥\n",
      "⎢-b + ╲╱  -4⋅a⋅c + b    -⎝b + ╲╱  -4⋅a⋅c + b  ⎠ ⎥\n",
      "⎢─────────────────────, ────────────────────────⎥\n",
      "⎣         2⋅a                     2⋅a           ⎦\n"
     ]
    }
   ],
   "source": [
    "a,b,c = sp.symbols('a b c')\n",
    "p = a*x**2 + b*x + c\n",
    "sp.pprint(p)\n",
    "print()\n",
    "sp.pprint(sp.solve(p, x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-1, 1, 1]\n"
     ]
    }
   ],
   "source": [
    "p = -1*x**2 + 1*x + 1\n",
    "P = sp.Poly(p,x).coeffs()\n",
    "print(P)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 1.61803399, -0.61803399])"
      ]
     },
     "execution_count": 65,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.roots(P)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'treu' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-66-58e7154d8d61>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mmpmpmpmpmpm\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mtreu\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m: name 'treu' is not defined"
     ]
    }
   ],
   "source": [
    "mpmpmpmpmpm = treu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 69,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "⎧   ⎛       _________⎞   _________ ⎛    _________    ⎞        _________⎫\n",
      "⎨x: ⎝-2 - ╲╱ -√3 + 2 ⎠⋅╲╱ -√3 + 2 ⋅⎝- ╲╱ -√3 + 2  + 2⎠, y: -╲╱ -√3 + 2 ⎬\n",
      "⎩                                                                      ⎭\n",
      "\n",
      "\n",
      "⎧    ⎛       _________⎞   _________ ⎛  _________    ⎞       _________⎫\n",
      "⎨x: -⎝-2 + ╲╱ -√3 + 2 ⎠⋅╲╱ -√3 + 2 ⋅⎝╲╱ -√3 + 2  + 2⎠, y: ╲╱ -√3 + 2 ⎬\n",
      "⎩                                                                    ⎭\n",
      "\n",
      "\n",
      "⎧   ⎛       ________⎞   ________ ⎛    ________    ⎞        ________⎫\n",
      "⎨x: ⎝-2 - ╲╱ √3 + 2 ⎠⋅╲╱ √3 + 2 ⋅⎝- ╲╱ √3 + 2  + 2⎠, y: -╲╱ √3 + 2 ⎬\n",
      "⎩                                                                  ⎭\n",
      "\n",
      "\n",
      "⎧    ⎛       ________⎞   ________ ⎛  ________    ⎞       ________⎫\n",
      "⎨x: -⎝-2 + ╲╱ √3 + 2 ⎠⋅╲╱ √3 + 2 ⋅⎝╲╱ √3 + 2  + 2⎠, y: ╲╱ √3 + 2 ⎬\n",
      "⎩                                                                ⎭\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "y = sp.Symbol(\"y\")\n",
    "sol = sp.solve([x**2 + y**2 - 4, x*y-1])\n",
    "for S in sol:\n",
    "        sp.pprint(S)\n",
    "        print('\\n')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
