import numpy as np
from functions import gauss


def sym_gauss_int_sqr(xb, n=10):
    """
    Return numeric Gauss integral via trap rule
    
    Parameters
    ---------
    
    xb : int
        input bounds
    
    n : int
        Number of equally spaced interval points. Default = 10.
    """
    x = np.linspace(-xb, xb, n+1)
    total = 0
    for i in range(n+1):
        try:
            average_height = (gauss(x[i], c=np.sqrt(1/2)) + gauss(x[i+1], c=np.sqrt(1/2)))/2
            total += average_height*(2*xb/n)
        except Exception as ex:
            pass  # no more trap rule iterations beyond right boundary
    
    return total**2
 