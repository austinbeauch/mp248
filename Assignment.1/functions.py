from numpy import exp


def gauss(x, a=1, b=0, c=1):
    """
    Evaluates the Gaussian function at a point
    
    Parameters
    ----------
    
    x : int
        input
    
    a : int
        vertical scale
    
    b : int
        x-axis offset
    
    c : int
        horizontal scale (standard deviation)
    """
    return a * exp(-(x - b)**2 / (2 * c**2))